#include "pincircuit.h"

#include <wiringPi.h>
#include <iostream>

PinCircuit::PinCircuit(int _pinOut,
                       int _pinIn)
{
    pinIn = _pinIn;
    pinOut = _pinOut;

    pinMode(pinIn, INPUT);
    pinMode(pinOut, OUTPUT);

    std::cout << "PullUp " << pinIn << std::endl;
    pullUpDnControl(pinIn, PUD_UP);

    if(pinOut != -1)
    {
        std::cout << "Set LOW " << pinOut << std::endl;
        digitalWrite(pinOut, LOW);
    }

    curStage = INIT;
}

bool PinCircuit::isCircuit() const
{
    if(digitalRead(pinIn) == HIGH)
        return false;
    else
        return true;
}

bool PinCircuit::isLastStage()
{
    switch(curStage)
    {
    case INIT:
    {
        if(isCircuit())
            curStage = INITED;
        return false;
    }

    case INITED:
    {
        if(!isCircuit())
            curStage = PUT_OFF;
        return false;
    }

    case PUT_OFF:
    {
        if(isCircuit())
        {
            curStage = PUT_ON;
            return true;
        }
        return false;
    }

    case PUT_ON:
        return true;
    }
}

void PinCircuit::reset()
{
    curStage = INIT;
}
