#include <iostream>
#include <wiringPi.h>
#include <stdlib.h>
#include <unistd.h>

#include "pincircuit.h"

using namespace std;

void info(const PinCircuit &pinCircuit, int pin1, int pin2);

int main()
{
    daemon(1, 1);

    wiringPiSetup();
    PinCircuit _reboot(PinCircuit::PIN35, PinCircuit::PIN36);
    PinCircuit _shutdown(PinCircuit::PIN39, PinCircuit::PIN40);

    info(_reboot, 35, 36);
    info(_shutdown, 39, 40);

    while(true)
    {
        if(_shutdown.isLastStage())
        {
            cout << "Shutdown" << endl;
            system("shutdown now");
            _shutdown.reset();
        }

        if(_reboot.isLastStage())
        {
            cout << "Reboot" << endl;
            system("shutdown -r now");
            _reboot.reset();
        }

        delay(500);
    }

    return 0;
}

void info(const PinCircuit &pinCircuit, int pin1, int pin2)
{
    if(pinCircuit.isCircuit())
        cout << pin1 << " <---> " << pin2 << endl;
    else
        cout << pin1 << " <-/-> " << pin2 << endl;
}
