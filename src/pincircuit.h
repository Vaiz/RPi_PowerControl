#ifndef PINCIRCUIT_H
#define PINCIRCUIT_H


class PinCircuit
{
public:
    enum Pins
    {
        PIN35 = 24,
        PIN36 = 27,
        PIN37 = 25,
        PIN38 = 28,
        PIN39 = -1, // Ground
        PIN40 = 29
    };

    enum Stages
    {
        INIT, // Ждем пока наденут джампер
        INITED, // Джампер надет
        PUT_OFF, // Джампер снят
        PUT_ON // Джампер одет обратно
    };

public:
    PinCircuit(int pinOut,
               int pinIn);
    bool isCircuit() const;
    bool isLastStage();
    void reset();

private:
    int pinIn, pinOut;
    Stages curStage;
};

#endif // PINCIRCUIT_H
